#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 11 22:25:22 2020
Under 4 seasons, use A* algorithm to find the fastest path (not have to be the 
shortest path) to orient in the Mendon Ponds Park based on specified three 
different courses. The park contains diverse terrians corresponding to different
speed and distinct elevation level.

@author: Sophie Hou (lh6032)
"""
from PIL import Image, ImageDraw
import math
import sys
import datetime


class Node:
    def __init__(self, x=0, y=0):
        """
        Use this class to store related information of each node.
        """
        self.x = x   # x coordinate
        self.y = y   # y coordinate
        
        self.parent=None   # parent node
        self.direction=None  # moved direction, for sum the total length of path
        self.g = 0   # g(n)
        self.h = 0   # h(n)
        self.f = 0   # f(n)
     
# spring:
# flood road (color: purpil,(148,0,211,255)):4.5
terrCostSpring = {(71,51,3,255):1, (0,0,0,255):1.5, (248,148,18,255):1.5, \
                      (255,255,255,255):2, (255,192,0,255):2.5, (2,208,60,255):3,\
                         (2,136,40,255):4, (148,0,211,255):4.5, (5,73,24,255):20, (0,0,255,255):20,\
                             (205,0,101,255):20}
# summer:
terrCostSummer = {(71,51,3,255):1, (0,0,0,255):1.5, (248,148,18,255):1.5, \
                      (255,255,255,255):2, (255,192,0,255):2.5, (2,208,60,255):3,\
                         (2,136,40,255):4,  (5,73,24,255):20, (0,0,255,255):20,\
                             (205,0,101,255):20}
# fall: 
# Easy movement forest: 2->2.5; Slow run forest: 3->3.5; Walk forest: 4->4.5
terrCostFall = {(71,51,3,255):1, (0,0,0,255):1.5, (248,148,18,255):1.5, \
                      (255,255,255,255):2.5, (255,192,0,255):2.5, (2,208,60,255):3.5,\
                         (2,136,40,255):4.5,  (5,73,24,255):20, (0,0,255,255):20,\
                             (205,0,101,255):20} 

# winter:
# ice road (color: cyan, (0,255,255,255)):4
terrCostWinter = {(71,51,3,255):1, (0,0,0,255):1.5, (248,148,18,255):1.5, \
                      (255,255,255,255):2, (255,192,0,255):2.5, (2,208,60,255):3,\
                         (2,136,40,255):4, (0,255,255,255):4, (5,73,24,255):20, (0,0,255,255):20,\
                             (205,0,101,255):20}
    
# the terrian cost of four seaons:
terrianCost = {'spring':terrCostSpring,\
               'summer': terrCostSummer,\
                   'fall': terrCostFall,\
                       'winter': terrCostWinter}

    
def readElevationFile(file):
    """
    Read input file: mpp.txt. And the elevation data in a 2d list. Note: the
    eleMap[x][y] matches to the pix map--pix[y,x].

    Parameters
    ----------
    file : filename, mpp.txt

    Returns
    -------
    eleMap : 2d list, elevation map

    """
    eleMap = []
    with open(file, 'r') as  eleFile:
        while True:
            line = eleFile.readline()
            if not line:
                break
            eleMap.append(line.split()[:-5]) # skip last 5 values per line
    return eleMap


def readCourseFile(file):
    """
    Read input file: brown.txt or red.txt or white.txt. Get controls of the
    event.
    
    Parameters
    ----------
    file : filename, brown.txt or red.txt or white.txt

    Returns
    -------
    pathGoals : list, a sequence of controls 
    """
    pathGoals = []
    with open(file, 'r') as courFile:
        while True:
            line = courFile.readline()
            if not line:
                break
            pos = line.strip().split()
            pathGoals.append((int(pos[0]),int(pos[1])))
    return pathGoals
    
    
def runAStar():
    """
    Run A * algorithm to find the fastest path from the start control to the 
    end control by calling searchPath(). And calculate and display the total 
    path length.

    Returns
    -------
    allPath : list, all the fastest path between every two controls in a course.

    """
    allPath = [] # to store all paths
    ttlLen= [] # store the total lenth of all path
    for i in range(0, len(pathGoals)-1):
        #print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Check  Point~~~~~~~~~~~~~~~~~~~~~~~")
        allPath.extend(searchPath(pathGoals[i], pathGoals[i+1], ttlLen))
    print("Total Path Length (in meters): ", sum(ttlLen))
    return allPath
        
        
def searchPath(iniPos, tarPos, ttlLen):
    """
    Implement the A * algorithm. Move to 4 directions: left, right, up and down.
    And treat each direction as a child Node, use Node to store important information.
    Calculate each node's g(n), h(n) and get their priority by comparing their 
    f(n).

    Parameters
    ----------
    iniPos : Node, the start position
    tarPos : Node, the target position
    ttlLen : [], to store the distance of all path

    Returns
    -------
    path : [], the fastest path between two controls.

    """
    queue = {}
    path = []
    path.append(iniPos) # (x,y)
    
    while True:
        pos = path[-1] # retrieve the last position
        
        if pos == tarPos: # exit loop, if find the target
            break
        pos = Node(pos[0], pos[1])  # create Node object
    
        if pos.x < 395 and pos.y < 500:
            # move up direction
            up = Node(pos.x, pos.y-1)  # (x,y-1)
            up.parent = pos
            up.direction = "up"
            up.g = pos.g + cal_gCost((pos.x,pos.y), (up.x,up.y), "up")
            up.h = cal_hCost((up.x,up.y), tarPos)
            up.f = up.g + up.h
            
            # move down direction
            down = Node(pos.x, pos.y+1) # (x,y+1)
            down.parent = pos
            down.direction = "down"
            down.g = pos.g + cal_gCost((pos.x,pos.y), (down.x,down.y), "down")
            down.h = cal_hCost((down.x,down.y), tarPos)
            down.f = down.g + down.h
            
            # move left direction
            left = Node(pos.x-1, pos.y) # (x-1,y)
            left.parent = pos
            left.direction = "left"
            left.g = pos.g + cal_gCost((pos.x,pos.y), (left.x,left.y), "left")
            left.h = cal_hCost((left.x,left.y), tarPos)
            left.f = left.g + left.h
            
            # move right direction
            right = Node(pos.x+1, pos.y) # (x+1,y)
            right.parent = pos
            right.direction = "right"
            right.g = pos.g + cal_gCost((pos.x,pos.y), (right.x,right.y), "right")
            right.h = cal_hCost((right.x,right.y), tarPos)
            right.f = right.g + right.h
            
            # store the children nodes in queue            
            if (up.x,up.y) not in path:
                queue[up] = up.f
            if (down.x,down.y) not in path:
                queue[down] = down.f
            if (left.x,left.y) not in path:
                queue[left] = left.f
            if (right.x,right.y) not in path:
                queue[right] = right.f
                
            # get the next position with the least f
            minPos = min(queue, key=queue.get)
            
            # get the moved distance 
            if minPos.direction == [ i for i in ["left", "right"]]:
                ttlLen.append(10.25)
            else:
                ttlLen.append(7.55)
            
            path.append((minPos.x,minPos.y))
            queue.pop(minPos)
    return path


def cal_hCost(nextPos, tarPos):
    """
    Calculates h(n) of each node. Uses the Euclidean Distance to estimate the 
    distance from the next node to the closest control.

    Parameters
    ----------
    nextPos : the next Node
    tarPos : the target Node

    Returns
    -------
    h_cost : int, h(n)
    """
    h_cost = (math.sqrt(((tarPos[0]-nextPos[0])*10.29)**2 + \
                        ((tarPos[1]-nextPos[1])*7.55)**2))                
    return h_cost


def cal_gCost(currPos, nextPos, direction):
    """
    Calculates g(n) of each node. g(n) means the actual distance between the 
    two consecutive nodes. g(n) = (terrian_cost + hill_cost ) * distance.
    Gets terrian_cost by checking the pixel color. Call hillCost() to get hill
    cost. 

    Parameters
    ----------
    currPos : current Node
    nextPos : next Node
    direction : left, right, up, down

    Returns
    -------
    g_cost : child g(n)
    """
    RGBval = pix[nextPos] # get RGB
    terr_Cost = terrianCost[season].get(RGBval)  # get terrian cost
    
    if direction == [ i for i in ["left", "right"]]:
        g_cost = (terr_Cost + hillCost(currPos, nextPos)) * 10.29 
    else:
        g_cost = (terr_Cost + hillCost(currPos, nextPos)) * 7.55 

    return g_cost
    

def hillCost(currPos, nextPos):
    """
    Calculates the speed of uphill, downhill and flat elevation.

    Parameters
    ----------
    currPos : current Node
    nextPos : next Node

    Returns
    -------
    hill_cost: int

    """
    if eleMap[currPos[1]][currPos[0]] == eleMap[nextPos[1]][nextPos[0]]: 
        return 0    # flat: no extra cost
    elif eleMap[currPos[1]][currPos[0]]< eleMap[nextPos[1]][nextPos[0]]: 
        return 1    # uphill: slow down
    else:
        return -0.5 # downhill: speed up
 

def searchLake():
    """
    Find the edge of lake/water.

    Returns
    -------
    lakeEdge : [], the edge of lake/water which are represented by pix position.

    """
    lakeEdge = []
    for i in range(0,394):
        for j in range(0,499):
            if pix[i,j] == (0,0,255,255) and\
                (pix[i,j+1] != (0,0,255,255) or\
                 pix[i,j-1] != (0,0,255,255) or \
                     pix[i+1,j] != (0,0,255,255)or\
                         pix[i-1,j] != (0,0,255,255)):
                    lakeEdge.append((i,j))
    return lakeEdge     
                 

def winterWalk(lakeEdge):
    """
    Change the 7 pixels around the non-water pixel into ice road (color: cyan,
    (0,255,255,255)), and store it inside the terrCostWinter with a speed cost
    of 4.

    Parameters
    ----------
    lakeEdge : [], the edge of lake/water
    Returns
    -------
    imageMap : Image, a new winter image map

    """
    while lakeEdge:
        (x,y) = lakeEdge.pop(0)
        for ind in range(7):
            if x+ind < 395 and pix[(x+ind,y)] == (0,0,255,255):
                pix[(x+ind,y)] = (0,255,255,255)
            elif x-ind >= 0 and pix[(x-ind,y)] == (0,0,255,255):
                pix[(x-ind,y)] = (0,255,255,255)
            elif y+ind < 499 and pix[(x,y+ind)] == (0,0,255,255):
                pix[(x,y+ind)] = (0,255,255,255)
            elif y-ind >= 0 and pix[(x,y-ind)] == (0,0,255,255):
                pix[(x,y-ind)] = (0,255,255,255)
    #imageMap.show()
    return imageMap


def springFlood(lakeEdge):
    """
    Change the 15 pixels around the water pixel with less than 1 meter
    difference from the water pixel into flood road (color: purpil,
    (148,0,211,255)),and store it inside the terrCostSpring with a speed cost
    of 4.5.

    Parameters
    ----------
    lakeEdge : [], the edge of lake/water
    Returns
    -------
    imageMap : Image, a new spring image map

    """
    while lakeEdge:
        (x,y) = lakeEdge.pop(0)
        for ind in range(15):
            if x+ind < 395 and float(eleMap[y][x])-float(eleMap[y][x+ind]) < 1 and pix[(x+ind,y)] != (0,0,255,255):
                pix[(x+ind,y)] = (148,0,211,255)
            elif x-ind >= 0 and float(eleMap[y][x])-float(eleMap[y][x-ind]) < 1 and pix[(x-ind,y)] != (0,0,255,255):
                pix[(x-ind,y)] = (148,0,211,255)
            elif y+ind < 499 and float(eleMap[y][x])-float(eleMap[y+ind][x]) < 1 and pix[(x,y+ind)] != (0,0,255,255):
                pix[(x,y+ind)] = (148,0,211,255)
            elif y-ind >= 0 and float(eleMap[y][x])-float(eleMap[y-ind][x]) < 1 and pix[(x,y-ind)] != (0,0,255,255):
                pix[(x,y-ind)] = (148,0,211,255)
    #imageMap.show()
    return imageMap
    
    
def writeOutFile(fileOut):
    """
    Draw lines of the shortest path with red color and Write out to a image file.

    Parameters
    ----------
    fileOut : output file

    Returns
    -------
    None.

    """        
    draw = ImageDraw.Draw(imageMap)
    for i in range(0, len(allPath)-1):
        draw.line([allPath[i],allPath[i+1]], fill = "red", width=2)
    imageMap.show()
    imageMap.save(fileOut)


def main(argv):
    start = datetime.datetime.now()
    
    global imageMap, pix, season, eleMap, pathGoals, allPath
    
    imageMap = Image.open(argv[1]) # open image map
    pix = imageMap.load()  # load all pix
    eleMap = readElevationFile(argv[2]) # store elevation map in 2d list
    pathGoals = readCourseFile(argv[3]) # get a sequence of controls
    season = argv[4] # get season
  
    lakeEdge = searchLake() # store the edge of lake/water
    if season == 'winter':
        print("WINTER")
        imageMap = winterWalk(lakeEdge) # get winter map 
    if season == 'spring':
        print("SPRING")
        imageMap = springFlood(lakeEdge) # get spring map
        
    allPath = runAStar() # run A* algorithm and get all path
    writeOutFile(argv[5]) # write out the fastest path on map
    
    runtime = datetime.datetime.now()-start # calculate runtime
    print("runtime: ", runtime)
    
 
if __name__=='__main__':
    main(sys.argv)
    
    

